#!/bin/bash

#parse json files

mkdir -p ./temp

for jobsFile in json/*; do
    for k in $(./jq -r '.jobs[].name' "$jobsFile"); do

        # get new status
        newUsercommand=$(./jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .command' "$jobsFile")
        [[ "$newUsercommand" == "null" ]] && { echo "$printDate: ERROR: Can't find command field on job $k in the $jobsFile file, the script will now exit." | tee -a "$logfile"; exit 1; }

        newSchedule=$(./jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .schedule' "$jobsFile")
        [[ "$newSchedule" == "null" ]] && { echo "$printDate: ERROR: Can't find schedule field on job $k in the $jobsFile file, the script will now exit." | tee -a "$logfile"; exit 1; }

        newSuspended=$(./jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .suspend' "$jobsFile")
        [[ "$newSuspended" == "null" ]] && { echo "$printDate: ERROR: Can't find suspend field on job $k in the $jobsFile file, the script will now exit." | tee -a "$logfile"; exit 1; }

        newDeadline=$(./jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .deadline' "$jobsFile")
        [[ "$newDeadline" == "null" ]] && { echo "$printDate: ERROR: Can't find deadline field on job $k in the $jobsFile file, the script will now exit." | tee -a "$logfile"; exit 1; }

        newImage=$(./jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .image' "$jobsFile")
        [[ "$newImage" == "null" ]] && { echo "$printDate: ERROR: Can't find image field on job $k in the $jobsFile file, the script will now exit." | tee -a "$logfile"; exit 1; }

        newTemplate=$(./jq -r --arg JOBNAME "$k" '.jobs[] | select(.name==$JOBNAME) | .template' "$jobsFile")
        [[ "$newTemplate" == "null" ]] && { echo "$printDate: ERROR: Can't find template field on job $k in the $jobsFile file, the script will now exit." | tee -a "$logfile"; exit 1; }
        
        cp templates/"$newTemplate" temp/"$k".yaml

        ESCAPED_COMMAND=$(printf '%s\n' "$newUsercommand" | sed -e 's/[\/&]/\\&/g')

        sa="dbjeedy"

	basenameJobsFile=$(basename $jobsFile)

        #override the job name
        sed -i 's#PLACEHOLDER_JOB_NAME#'"$k"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_SCHEDULE#'"$newSchedule"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_USER_COMMAND#'"$ESCAPED_COMMAND"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_DEADLINE#'"$newDeadline"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_SUSPENDED#'"$newSuspended"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_USER#'"$sa"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_IMAGE#'"$newImage"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_TEMPLATE#'"$newTemplate"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_JOBS_FILE#'"$basenameJobsFile"'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_NFS_CLAIM_NAME#'"$sa"-nfs-claim'#' temp/"$k".yaml
        sed -i 's#PLACEHOLDER_NAMESPACE#'"$sa"-ns'#' temp/"$k".yaml
	sed -i 's#PLACEHOLDER_SA#'"$sa"-sa'#' temp/"$k".yaml
        
    done
done

touch kustomization.yaml
kustomize edit fix >/dev/null 2>&1
kustomize edit add resource temp/* >/dev/null 2>&1
kustomize edit add configmap "$basenameJobsFile" --from-file=json/"$basenameJobsFile" --disableNameSuffixHash >/dev/null 2>&1
kustomize edit fix >/dev/null 2>&1
kustomize build .
