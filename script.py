import json
import os
import yaml
import shutil
from pathlib import Path



p = Path('.')

path_json = p / "json"
path_templates = p / "templates"

Path(str(p) + "/" + "temp").mkdir(exist_ok=True)

path_temp = p / "temp"

#print(path_json)

#os.chdir('templates')
#os.mkdir('temp')
#with open('base.yaml') as file:
#    # The FullLoader parameter handles the conversion from YAML
#    # scalar values to Python the dictionary format
#    yaml_file = yaml.safe_load(file)

#os.chdir("..")
#os.chdir(path_json)

key_list = []

#os.chdir(str(path_json))
for file in os.listdir(str(path_json)):
#for file in os.listdir():
#   os.chdir("..") 
   with open(str(path_json) + "/" + file, "r") as read_file:
        json_file = json.load(read_file)
        try:
            for job in json_file['jobs']:
                job_name = job['name']
                #with open(str(path_templates) + "/" + job['template'], "r") as read_template:
                #    yaml_file = yaml.safe_load(read_template)
                jobTemplate = job['template']
                #newTemplate = path_temp / "%s.yaml" %job_name
                job_name_yaml = job_name + ".yaml"
                newTemplate = Path.joinpath(path_temp, job_name_yaml)
                shutil.copyfile(str(path_templates) + "/" + jobTemplate, newTemplate)
                newTemplate.write_text(newTemplate.read_text().replace('PLACEHOLDER_SCHEDULE', job['schedule']))
                newTemplate.write_text(newTemplate.read_text().replace('PLACEHOLDER_COMMAND', job['command']))
                newTemplate.write_text(newTemplate.read_text().replace('PLACEHOLDER_SUSPENDED', job['suspend']))
                newTemplate.write_text(newTemplate.read_text().replace('PLACEHOLDER_DEADLINE', job['deadline']))
                newTemplate.write_text(newTemplate.read_text().replace('PLACEHOLDER_IMAGE', job['image']))
                newTemplate.write_text(newTemplate.read_text().replace('PLACEHOLDER_JOB_NAME', job['name']))
                newTemplate.write_text(newTemplate.read_text().replace('PLACEHOLDER_JOBS_FILE', file))
                newTemplate.write_text(newTemplate.read_text().replace('PLACEHOLDER_TEMPLATE', job['template']))
                #create_job_yamlfile
                #Check if job name exists already
                #if job.keys() in key_list:
                #   print("This job exists already:", job.keys())
                #else:
                #    key_list.append(job.keys())
                print("*********************")
        except Exception as e:
            print(e)
            print("********************")
#print(key_list)
    #    os.chdir("..")
    #    os.chdir(path_templates)
    #    for job_name in json_file['jobs']:
    #        print(job_name)